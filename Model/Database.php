<?php
require_once  "../inc/config.php";
class Database
{
    protected $connection = null;

    public function __construct()
    {
        try {
            $this->connection = new mysqli(DB_HOST, DB_USERNAME, DB_PASSWORD, DB_DATABASE_NAME);
    	
            if ( mysqli_connect_errno()) {
                throw new Exception("Could not connect to database.");   
            }
        } catch (Exception $e) {
            throw new Exception($e->getMessage());   
        }			
    }


    public function getPrepareConnection($query = ""){
        try{
            $stmt = $this->connection->prepare( $query );
            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " . $query);
            }
            return $stmt;
        } catch (Exception $e) {
            throw new Exception($e->getMessage());   
        }     
    }



    public function select($stmt )
    {
        try {
            $stmt = $this->executeStatement( $stmt );
            $result = $stmt->get_result()->fetch_all(MYSQLI_ASSOC);				
            $stmt->close();

            return $result;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }
        return array();
    }

    public function update_delete($stmt){
        try {
            $stmt = $this->executeStatement( $stmt );
            $result = $stmt->affected_rows;				
            $stmt->close();

            return $result;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }
        return 0;
    }

    public function insert($stmt){
        try {
            $stmt = $this->executeStatement( $stmt );
            $result = $stmt->insert_id;				
            $stmt->close();

            return $result;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }
        return 0;
    }

    private function executeStatement($stmt)
    {
        try {
            

            if($stmt === false) {
                throw New Exception("Unable to do prepared statement: " );
            }
            $stmt->execute();

            return $stmt;
        } catch(Exception $e) {
            throw New Exception( $e->getMessage() );
        }	
    }


}