<?php
require_once  "Database.php";
require_once "DetallePedido.php";

class Pedido extends Database {

    private $detallePedido = array();
    private $idPedido;
    private $fecha;
    private $idUsuario;
    private $idEstado_pedido;

    public function getPedido($idUsuario){
        $stmt = $this->getPrepareConnection("SELECT * FROM pedido WHERE idUsuario = ? and idEstado_pedido not in (1)");
        $stmt->bind_param("i", $idUsuario);
        $result = $this->select($stmt);
        return $result;
    }

    public function addPedido($idUser,$detallePedido){
        $stmt = $this->getPrepareConnection("INSERT INTO pedido (fecha, idUsuario, idEstado_pedido) VALUES (now(), ?, ?)");
        $estadoPedido= 1;
        $stmt->bind_param("si",$idUser,$estadoPedido);
        $pedidoId = $this->insert($stmt);
        if($pedidoId && $pedidoId>0){
            foreach($detallePedido as $detalle){
                $producto = $detalle["idProducto"];
                $cantidad = $detalle["cantidad"];
                
                $stmt2 = $this->getPrepareConnection("INSERT INTO detalle_pedido (idPedido, idProducto, cantidad) VALUES (?, ?, ?)");
                $stmt2->bind_param("iii",$pedidoId,$producto,$cantidad);
                $this->insert($stmt2);

                $stmt2 = $this->getPrepareConnection("UPDATE PRODUCTO SET cantidadProducto = cantidadProducto-? where idProducto = ?");
                $stmt2->bind_param("ii",$cantidad,$producto);
                $this->update_delete($stmt2);
            }
        }

        return $pedidoId;
    }

    public function addDetallePedido(){

    }

    public function updatePedido(){

    }

    public function finalizarPedido(){

    }

    /**
     * Get the value of idPedido
     */ 
    public function getIdPedido()
    {
        return $this->idPedido;
    }

    /**
     * Set the value of idPedido
     *
     * @return  self
     */ 
    public function setIdPedido($idPedido)
    {
        $this->idPedido = $idPedido;

        return $this;
    }

    /**
     * Get the value of fecha
     */ 
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * Set the value of fecha
     *
     * @return  self
     */ 
    public function setFecha($fecha)
    {
        $this->fecha = $fecha;

        return $this;
    }

    /**
     * Get the value of idUsuario
     */ 
    public function getIdUsuario()
    {
        return $this->idUsuario;
    }

    /**
     * Set the value of idUsuario
     *
     * @return  self
     */ 
    public function setIdUsuario($idUsuario)
    {
        $this->idUsuario = $idUsuario;

        return $this;
    }

    /**
     * Get the value of idEstado_pedido
     */ 
    public function getIdEstado_pedido()
    {
        return $this->idEstado_pedido;
    }

    /**
     * Set the value of idEstado_pedido
     *
     * @return  self
     */ 
    public function setIdEstado_pedido($idEstado_pedido)
    {
        $this->idEstado_pedido = $idEstado_pedido;

        return $this;
    }
}