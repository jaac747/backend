<?php
require_once  "Database.php";

class Producto extends Database {

    private $idProducto;
    private $nombreProducto;
    private $detalleProducto;
    private $precioUnitario;
    private $cantidadProducto;
    private $proveedorProducto;

    public function getAllProductos(){
        $stmt = $this->getPrepareConnection("SELECT * FROM `producto`");
        $result = $this->select($stmt);
        return $result;
    }

    public function getProducto($id){
        $stmt = $this->getPrepareConnection("SELECT * FROM `producto` where idProducto = ?");
        $stmt->bind_param("s", $id);
        $result = $this->select($stmt);
        return $result;
    }

    public function updateProduct(){

    }

    /**
     * Get the value of idProducto
     */ 
    public function getIdProducto()
    {
        return $this->idProducto;
    }

    /**
     * Set the value of idProducto
     *
     * @return  self
     */ 
    public function setIdProducto($idProducto)
    {
        $this->idProducto = $idProducto;

        return $this;
    }

    /**
     * Get the value of nombreProducto
     */ 
    public function getNombreProducto()
    {
        return $this->nombreProducto;
    }

    /**
     * Set the value of nombreProducto
     *
     * @return  self
     */ 
    public function setNombreProducto($nombreProducto)
    {
        $this->nombreProducto = $nombreProducto;

        return $this;
    }

    /**
     * Get the value of detalleProducto
     */ 
    public function getDetalleProducto()
    {
        return $this->detalleProducto;
    }

    /**
     * Set the value of detalleProducto
     *
     * @return  self
     */ 
    public function setDetalleProducto($detalleProducto)
    {
        $this->detalleProducto = $detalleProducto;

        return $this;
    }

    /**
     * Get the value of precioUnitario
     */ 
    public function getPrecioUnitario()
    {
        return $this->precioUnitario;
    }

    /**
     * Set the value of precioUnitario
     *
     * @return  self
     */ 
    public function setPrecioUnitario($precioUnitario)
    {
        $this->precioUnitario = $precioUnitario;

        return $this;
    }

    /**
     * Get the value of cantidadProducto
     */ 
    public function getCantidadProducto()
    {
        return $this->cantidadProducto;
    }

    /**
     * Set the value of cantidadProducto
     *
     * @return  self
     */ 
    public function setCantidadProducto($cantidadProducto)
    {
        $this->cantidadProducto = $cantidadProducto;

        return $this;
    }

    /**
     * Get the value of proveedorProducto
     */ 
    public function getProveedorProducto()
    {
        return $this->proveedorProducto;
    }

    /**
     * Set the value of proveedorProducto
     *
     * @return  self
     */ 
    public function setProveedorProducto($proveedorProducto)
    {
        $this->proveedorProducto = $proveedorProducto;

        return $this;
    }

    
}

?>